using System;
using Microsoft.EntityFrameworkCore;

namespace ProyectoDeSoftware
{
    public class POSDbContext : DbContext {
        //Constructor sin parametros
        public POSDbContext() {
        }

        public POSDbContext(DbContextOptions options) : base(options) {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("User Id=proyectodesoftware;Password=proyectodesoftware;Server=localhost;Port=5434;Database=proyectodesoftware;Integrated Security=true;Pooling=true;");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Producto>()
                .HasIndex(b => b.Codigo)
                .IsUnique();
        
            // modelBuilder.Entity<Ventas>(entity => { 
            //     entity.HasOne(d => d.Cliente);
            // });

            modelBuilder.Entity<Producto>().HasData(new Producto {
                ProductoId = 1, Codigo = "0001", Marca = "Amanda", Nombre = "Yerba Mate x Kilo", Precio = (Decimal)150.99});
            modelBuilder.Entity<Producto>().HasData(new Producto {
                ProductoId = 2, Codigo = "0002", Marca = "Velez", Nombre = "Té en saquitos", Precio = (Decimal)50.00});
            modelBuilder.Entity<Producto>().HasData(new Producto {
                ProductoId = 3, Codigo = "0003", Marca = "La Serenisima", Nombre = "Leche", Precio = (Decimal)40.20});
            modelBuilder.Entity<Producto>().HasData(new Producto {
                ProductoId = 4, Codigo = "0004", Marca = "La Serenisima", Nombre = "Dulce de Leche", Precio = (Decimal)76.00});
            modelBuilder.Entity<Producto>().HasData(new Producto {
                ProductoId = 5, Codigo = "0005", Marca = "Amanda", Nombre = "Yerba Mate x 1/2 Kilo", Precio = (Decimal)80.99});                                                
        }

        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Ventas> Ventas { get; set; }
    }
}
