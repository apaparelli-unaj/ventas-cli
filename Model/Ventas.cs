using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;


namespace ProyectoDeSoftware {
    public class Ventas {
        public int VentasId { get; set; }
        public int ClienteId { get; set; } 
        public int ProductoId { get; set; } 
        public DateTime Fecha { get; set; }

        public virtual Cliente Cliente { get; set; }
        public virtual Producto Producto { get; set; }

        public static void agregarVenta(Ventas venta) {
            using (var db = new POSDbContext()) {
                db.Ventas.Add(venta);
                db.SaveChanges();
            }
        }

        public static List<Ventas> listaVentasDia() {
            using (var db = new POSDbContext()) {
                var today = DateTime.Today;
                return db.Ventas
                    .Include(t => t.Cliente)
                    .Where(t => t.Fecha.Date >= today)
                    .OrderBy(t => t.Fecha).ToList();
            }
        }

        public static List<Ventas> listaVentasDia(int producto) {
            using (var db = new POSDbContext()) {
                var today = DateTime.Today;
                return db.Ventas
                    .Include(t => t.Cliente)
                    .Where(t => t.Fecha.Date >= today)
                    .Where(t => t.ProductoId == producto)
                    .OrderBy(t => t.Fecha).ToList();
            }
        }
    }
}
