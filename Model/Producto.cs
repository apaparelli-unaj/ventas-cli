using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace ProyectoDeSoftware
{
    public class Producto {
        public Producto() {
            this.Ventas = new HashSet<Ventas>();
        }
        public int ProductoId { get; set; }
        [StringLength(45)]
        public string Codigo { get; set; }
        [StringLength(45)]
        public string Marca { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        [Column(TypeName = "decimal(15,2)")]
        public decimal Precio { get; set; }

        public virtual ICollection<Ventas> Ventas { get; set; } 

        public static List<Producto> listarProductos() {
            using (var db = new POSDbContext()) {
                return  db.Producto.ToList();
            }
        }

        public static Producto obtenerProductoPorID(int id) {
            using (var db = new POSDbContext()) {
                return db.Producto.First(c => c.ProductoId == id);
            }
        }

    }
}
