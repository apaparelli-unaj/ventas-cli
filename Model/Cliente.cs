using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoDeSoftware
{
    
    public class Cliente {
        public Cliente() {
            this.Ventas = new HashSet<Ventas>();
        }

        public int ClienteId { get; set; }
        [StringLength(10)]
        public string DNI { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        [StringLength(45)]
        public string Apellido { get; set; }
        [StringLength(45)]
        public string Direccion { get; set; }
        [StringLength(45)]
        public string Telefono { get; set; }

        public virtual ICollection<Ventas> Ventas { get; set; }

        public static List<Cliente> listarClientes() {
            using (var db = new POSDbContext()) {
                return  db.Cliente.ToList();
            }
        }

        public static Cliente obtenerClientePorID(int id) {
            using (var db = new POSDbContext()) {
                return db.Cliente.First(c => c.ClienteId == id);
            }
        }

        public static void agregarCliente(Cliente cliente) {
            using (var db = new POSDbContext()) {
                db.Cliente.Add(cliente);
                db.SaveChanges();
            }
        }

    }
    
}
