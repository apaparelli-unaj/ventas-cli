﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace practica_orm.Migrations
{
    public partial class removeExtraFieldsToVentas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdCliente",
                table: "Ventas");

            migrationBuilder.DropColumn(
                name: "IdProducto",
                table: "Ventas");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdCliente",
                table: "Ventas",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdProducto",
                table: "Ventas",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
