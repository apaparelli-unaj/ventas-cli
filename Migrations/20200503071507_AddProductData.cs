﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace practica_orm.Migrations
{
    public partial class AddProductData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Producto",
                columns: new[] { "ProductoId", "Codigo", "Marca", "Nombre", "Precio" },
                values: new object[,]
                {
                    { 1, "0001", "Amanda", "Yerba Mate x Kilo", 150.99m },
                    { 2, "0002", "Velez", "Té en saquitos", 50m },
                    { 3, "0003", "La Serenisima", "Leche", 40.2m },
                    { 4, "0004", "La Serenisima", "Dulce de Leche", 76m },
                    { 5, "0005", "Amanda", "Yerba Mate x 1/2 Kilo", 80.99m }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Producto",
                keyColumn: "ProductoId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Producto",
                keyColumn: "ProductoId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Producto",
                keyColumn: "ProductoId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Producto",
                keyColumn: "ProductoId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Producto",
                keyColumn: "ProductoId",
                keyValue: 5);
        }
    }
}
