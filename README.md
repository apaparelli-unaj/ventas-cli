# Trabajo Práctico 1

## Parte 1 - ORM

[Descargar consigna](docs/PracticaORM_parte_1.pdf)

Crear el proyecto

    dotnet new console -o practica_orm
    cd practica_orm

Agregar el paquete para PostgreSQL y otros:

    dotnet add package Microsoft.EntityFrameworkCore.Tools
    dotnet add package Microsoft.EntityFrameworkCore.Design
    dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL


Crear base de datos

    dotnet new tool-manifest
    dotnet tool install --local dotnet-ef
    dotnet add package Microsoft.EntityFrameworkCore.Design
    dotnet add package EntityFramework
    dotnet ef migrations add InitialCreate
    dotnet ef database update


Comandos de dotnet-ef:

    dotnet ef migrations add
    dotnet ef migrations list
    dotnet ef migrations script
    dotnet ef dbcontext info
    dotnet ef dbcontext scaffold
    dotnet ef database drop
    dotnet ef database update


Links:

https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli
https://docs.microsoft.com/en-us/ef/core/

https://apuntes-snicoper.readthedocs.io/es/latest/programacion/csharp/dotnet_core/dotnet_core_postgresql.html

Levantar Docker de PostgreSQL

    docker-compose up -d

Borrar todos los contenedores

    docker rm $(docker ps -a -q)

Borrar todas las imágenes

    docker rmi $(docker images -q)