﻿using System;
using System.Collections.Generic;


namespace ProyectoDeSoftware {
    class Program {

        static void Main(string[] args) {
            bool showMenu = true;
            while (showMenu) {
                showMenu = MainMenu();
            }
        }

        private static bool MainMenu() {
            Console.Clear();
            Console.WriteLine("Opciones:");
            Console.WriteLine("1) Nueva venta");
            Console.WriteLine("2) Nuevo Cliente");
            Console.WriteLine("3) Listado de ventas del día");
            Console.WriteLine("4) Salir");
            Console.Write("\r\nSeleccione una opción: ");
        
            switch (Console.ReadLine())
            {
                case "1":
                    NuevaVenta();
                    return true;
                case "2":
                    NuevoCliente();
                    return true;
                case "3":
                    VentasDia();
                    return true;
                case "4":
                    return false;
                default:
                    return true;
            }
        }

        private static bool VentasDia() {
            Console.Clear();
            Console.WriteLine("Opciones de ventas:");
            Console.WriteLine("1) Listar todas las ventas del día");
            Console.WriteLine("2) Buscar producto en ventas del día");
            Console.WriteLine("3) Volver al menú principal");
            Console.Write("\r\nSeleccione una opción: ");
        
            switch (Console.ReadLine())
            {
                case "1":
                    ListadoVentasDia();
                    return true;
                case "2":
                    ListadoVentasDiaPorProducto();
                    return true;
                case "3":
                    return false;
                default:
                    return true;
            }  
        }

        private static void ListadoVentasDia() {
            Console.Clear();

            var ventas = Ventas.listaVentasDia();
            imprimirTablaVentas(ventas);
        }

        private static void ListadoVentasDiaPorProducto() {
            Console.Clear();
            Console.Write("Ingresar El id de un producto para filtrar: ");
            int producto = Convert.ToInt32(Console.ReadLine());

            var ventas = Ventas.listaVentasDia(producto);

            imprimirTablaVentas(ventas);
        }

        private static void imprimirTablaVentas(List<Ventas> ventas) {
            if (ventas.Count == 0){
                Console.WriteLine("No hay ventas registradas en el día de hoy");   
            }
            else {            
                string text = "{0, -15}{1, -15}{2, -20}{3, -30}{4, -10}{5, -15}";
                Console.WriteLine(text, "Nombre", "Apellido", "Marca", "Nombre Producto", "Precio", "Fecha");
                Console.WriteLine(text, "------", "--------", "-----", "---------------", "------", "-----");
                foreach(Ventas v in ventas) {
                    // Esto lo hago porque no me funciono v.Cliente.Nombre
                    Cliente cliente = Cliente.obtenerClientePorID(v.ClienteId);
                    Producto producto = Producto.obtenerProductoPorID(v.ProductoId);

                    Console.WriteLine(text,
                            cliente.Nombre,
                            cliente.Apellido,
                            producto.Marca,
                            producto.Nombre,
                            producto.Precio,
                            v.Fecha
                        );
                }
            }
            Console.Write("Presione cualquier Volver al menú");
            Console.ReadKey();  
        }

        private static void NuevoCliente() {
            Console.Clear();
            Console.Write("Ingresar DNI: ");
            string dni = Console.ReadLine();
            Console.Write("Ingresar Nombre: ");
            string nombre = Console.ReadLine();
            Console.Write("Ingresar Apellido: ");
            string apellido = Console.ReadLine();
            Console.Write("Ingresar Direccion: ");
            string direccion = Console.ReadLine();
            Console.Write("Ingresar Teléfono: ");
            string telefono = Console.ReadLine();
            var cliente = new Cliente() {
                    DNI = dni,
                    Nombre = nombre,
                    Apellido = apellido,
                    Direccion = direccion,
                    Telefono = telefono
                };

            Cliente.agregarCliente(cliente);

            Console.Write("Cliente guardado. Presione cualquier tecla para continuar");
            Console.ReadKey();  
        }
        private static void NuevaVenta() {
            Console.Clear();
            Console.Write("Ingresar id de cliente: ");
            int cliente_id = Convert.ToInt32(Console.ReadLine());
            Cliente cliente = Cliente.obtenerClientePorID(cliente_id);
            Console.WriteLine("Cliente: {0} {1}", cliente.Nombre, cliente.Apellido);

            Console.Write("Ingresar id del producto: ");
            int producto_id = Convert.ToInt32(Console.ReadLine());
            Producto producto = Producto.obtenerProductoPorID(producto_id);
            Console.WriteLine("Producto: {0} {1} {2}", producto.Marca, producto.Nombre, producto.Precio);
            
            var venta = new Ventas() {
                    Fecha = DateTime.Now,
                    ClienteId = cliente_id,
                    ProductoId = producto_id
                };
            Ventas.agregarVenta(venta);

            Console.Write("Venta registrada. Presione cualquier tecla para continuar");
            Console.ReadKey();  
        }

            // var productos = Producto.listarProductos();
            // foreach(Producto p in productos) {
            //     Console.WriteLine("{0} {1}",p.ProductoId, p.Nombre);
            // }

            // var clientes = Cliente.listarClientes();
            // foreach(Cliente c in clientes) {
            //     Console.WriteLine("{0} {1} {2}", c.ClienteId, c.Nombre, c.Apellido);
            // }

            // // var cliente = new Cliente() {
            // //         DNI = "10123123",
            // //         Nombre = "Arturo",
            // //         Apellido = "JAuretche",
            // //         Direccion = "Av. Calchaqui 1234",
            // //         Telefono = "4444-4444"
            // //     };
            // // Cliente.agregarCliente(cliente);

            
            // Cliente cliente = Cliente.obtenerClientePorID(1);
            // // Console.WriteLine("{0} {1} {2}", cliente.ClienteId, cliente.Nombre, cliente.Apellido);
            // Producto producto = Producto.obtenerProductoPorID(1);
            // // Console.WriteLine("{0} {1} {2}", producto.ProductoId, producto.Nombre, producto.Precio);


            // var venta = new Ventas() {
            //         Fecha = DateTime.Now,
            //         Cliente = cliente,
            //         Producto = producto
            //     };
            // Ventas.agregarVenta(venta);
        
    }

}
